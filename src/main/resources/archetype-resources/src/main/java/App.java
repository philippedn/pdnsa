package $package;

import static java.util.Arrays.stream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;
import lombok.extern.java.Log;

@Log
public class App {

  public static void main(String[] args) {
    stream(getVersion()).forEach(log::info);
  }

  private static String[] getVersion() {
    var url = Optional.ofNullable(App.class.getResource("/git.properties"));

    return url.flatMap(App::toInputStream)
        .flatMap(App::toProperties)
        .map(App::toVersion)
        .orElseGet(App::toUnknown);
  }

  private static Optional<InputStream> toInputStream(URL url) {
    try {
      return Optional.of(url.openStream());
    } catch (IOException ex) {
      return Optional.empty();
    }
  }

  private static Optional<Properties> toProperties(InputStream inputStream) {
    try {
      var properties = new Properties();
      properties.load(inputStream);
      return Optional.of(properties);
    } catch (IOException ex) {
      return Optional.empty();
    }
  }

  private static String[] toVersion(Properties properties) {
    return new String[] {
      "release: " + properties.getProperty("git.build.version"),
      "build: " + properties.getProperty("git.commit.id.abbrev"),
      "date: " + properties.getProperty("git.build.time")
    };
  }

  private static String[] toUnknown() {
    return new String[] {"unknown"};
  }
}
